#!/bin/bash
source src/instala_dialog.sh
source src/funciones_comunes.sh
source src/funciones_apt.sh
source src/funciones_dnf.sh
source src/funciones_pacman.sh
source src/funciones_zypper.sh

param=$1
version="4.0"
if [ "$param" = "--version" ] || [ "$param" = "-v" ]; then
        echo "Neteja versión ${version}"
elif [ "$param" = "--help" ] || [ "$param" = "-h" ]; then
        ayuda
else
        titulo="https://gitlab.com/voromv/Neteja"
        preDialog
        instala_dialog
        despedida
fi
