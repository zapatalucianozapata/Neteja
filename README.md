## NUEVO REPO!!!!!!!
<p>Este repo no recibira mas actualizaciones</p>

<p>Nos mudamos al repo de la comunidad, para organizar mejor nuestro traba</p>

<p>Acceda desde aqui al nuevo repositorio: </p><a href="https://gitlab.com/Comunidad-VoroMV/Neteja">Neteja</a>

<p>O a traves del siguiente link https://gitlab.com/Comunidad-VoroMV/Neteja</p>

<p>Un Saludo</p>
<p>Los admins :)</p>

# Neteja
 
</p>Este es el Script de Voro MV, para limpiar el sistema del pingüino, es válido para los gestores de paquetes <STRONG>APT, DNF, PACMAN y ZYPPER</STRONG>.</p>
<p>Se llama <STRONG>NETEJA</STRONG> y está en su 4ª versión. Con él, vamos a actualizar el sistema , Flatpak y/o Snap, si los tienes instalados. Finalmente eliminaremos toda la basurilla de nuestro sistema operativo.</p>
 
### Nota:
En el proceso, tendrás que poner tu contraseña de usuario.
 
 
## Funcionamiento del Script
 
<p>Para utilizar Neteja lo único que tendrás que hacer es descargarlo en un directorio de tu sistema y darle permiso de ejecución al fichero neteja.sh.</p>
<p>Puedes hacerlo desde las propiedades del fichero o mediante la terminal con el siguiente comando.</p>

```bash
chmod +x neteja.sh
```

<p>
Al ejecutar el script sin parámetros empezará a identificar qué gestor de paquetes tiene tu sistema y empezará a actualizarlo y a limpiarlo después de que le indiques la contraseña del usuario root.
</p>
 
```bash
./neteja.sh #Sin parámetros actualizará y limpiará el sistema
```
<p>El script cuenta con dos parámetros, como podemos ver en la explicación siguiente. Cuando se le indica los parámetros solo dará la información de la versión del script y una pequeña ayuda, pero en ningún caso actualizará ni limpiará el sistema.</p>
 
```bash
./neteja.sh [-v][--version] [-h][--help]
 
# [-v][--version]: Argumento opcional que indica la versión del script.
 
# [-h][--help]: Argumento opcional que muestra la ayuda del script.
```
