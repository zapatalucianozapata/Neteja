function fDnf() {
  dialog --title $titulo --msgbox " 
     Actualizamos el sistema.
    
     También Flatpak y Snap, si los tienes instalados. 
     " 0 0

  dialog --title $titulo --infobox "Actualizando el sistema..." 0 0
  sudo dnf update -y >/dev/null 2>&1
  sudo flatpak update >/dev/null 2>&1
  sudo snap refresh >/dev/null 2>&1
}

function fDnfCache {
  dialog --title $titulo --msgbox "
    También borramos versiones antiguas de programas, 
    Kernel, cachés, repositorios no usados, 
    paquetes huerfanos...
    
    " 0 0

  dialog --title $titulo --infobox "Borrando cachés, etc..." 0 0
  sudo dnf clean all >/dev/null 2>&1
  sudo dnf autoremove >/dev/null 2>&1
}