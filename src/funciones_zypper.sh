function fZypper() {
    dialog --title $titulo --msgbox " 
     Actualizamos el sistema.
    
     También Flatpak y Snap, si los tienes instalados. 
     " 0 0

    dialog --title $titulo --infobox "Actualizando el sistema..." 0 0
    sudo zypper update -y >/dev/null 2>&1
    flatpak upgrade -y >/dev/null 2>&1
}

function fZypperCache {
    dialog --title $titulo --msgbox "
    También borramos versiones antiguas de programas, 
    Kernel, cachés, repositorios no usados, 
    paquetes huerfanos...
    
    " 0 0

    dialog --title $titulo --infobox "Borrando cachés, etc..." 0 0
    sudo zypper clean >/dev/null 2>&1
}