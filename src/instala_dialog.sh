function instala_dialog() {

####  AVERIGUAMOS QUÉ PAQUETE USA E INSTALA DIALOG SI NO EXISTE
#       1: APT    para Debian, Ubuntu y derivadas
#       2: DNF    para Fedora
#       3: PACMAN para Arch
#       4: ZYPPER para Suse 
#       n: Salir

        sudo dialog --help >/dev/null 2>&1
        if [ $? == 0 ]; 
                then i_dialog=0
                else i_dialog=1
        fi 
        
        sudo apt --help >/dev/null 2>&1
                if [ $? == 0 ]; then
                        if [ $i_dialog == 1 ]; then sudo apt install dialog 
                        fi
                        sn=1
		 	dialog --title $titulo --yesno "Tu sistema tiene como gestor de paquetes APT ¿Quieres continuar?" 0 0
                	if [ $? == 1 ]; 
                         then
                   	    clear 
                   	    exit 
                	fi
                 fi

        sudo dnf --help >/dev/null 2>&1
                if [ $? == 0 ]; then
                        if [ $i_dialog == 1 ]; then sudo dnf install dialog 
                        fi
                        sn=2
		        dialog --title $titulo --yesno "Tu sistema tiene como gestor de paquetes DNF ¿Quieres continuar?" 0 0       		
                	if [ $? == 1 ]; 
                         then
                   	    clear 
                   	    exit 
                	fi
                 fi

        sudo pacman --help >/dev/null 2>&1
                if [ $? == 0 ]; then
                        if [ $i_dialog == 1 ]; then sudo pacman -S dialog --noconfirm
                        fi
                        sn=3
                        presentacion
		        dialog --title $titulo --yesno "Tu sistema tiene como gestor de paquetes PACMAN ¿Quieres continuar?" 0 0
                	if [ $? == 1 ]; 
                         then
                   	    clear 
                   	    exit 
                	fi
                fi

        sudo zypper --help >/dev/null 2>&1
                if [ $? == 0 ]; then
                        if [ $i_dialog == 1 ]; then sudo zypper install dialog 
                        fi
                        sn=4
                        presentacion
		        dialog --title $titulo --yesno "Tu sistema tiene como gestor de paquetes ZYPPER ¿Quieres continuar?" 0 0
                	if [ $? == 1 ]; 
                         then
                   	    clear 
                   	    exit 
                	fi
                fi

        que_hacer=$(dialog --title $titulo --stdout --checklist "Dinos qué quieres hacer" 0 0 3\
                        1 "Actualizar el sistema" on\
                        2 "Borrar caché, temporales, ..." on\
                        3 "Borrar la papelera" on)

        case $sn in
                [1]*)
                        if [[ $que_hacer =~ "1" ]] 
                                then fApt
                        fi
                        if [[ $que_hacer =~ "2" ]] 
                                then fAptCache
                        fi
                        ;;
                [2]*)
                        if [[ $que_hacer =~ "1" ]] 
                                then fDnf
                        fi
                        if [[ $que_hacer =~ "2" ]] 
                                then fDnfCache
                        fi
                        ;;
                [3]*)
                        if [[ $que_hacer =~ "1" ]] 
                                then fPacman
                        fi
                        if [[ $que_hacer =~ "2" ]] 
                                then fPacmanCache
                        fi
                        ;;
                [4]*)
                        if [[ $que_hacer =~ "1" ]] 
                                then fZypper
                        fi
                        if [[ $que_hacer =~ "2" ]] 
                                then fZypperCache
                        fi
                        ;;
                [Nn]*) exit ;;
                *) echo "Tu gestor de paquetería no es compatible con el scritp Neteja." ;;
        esac

        if [[ $que_hacer =~ "2" ]] 
                then dirTemporales
        fi
        if [[ $que_hacer =~ "3" ]] 
                then papelera
        fi
  }