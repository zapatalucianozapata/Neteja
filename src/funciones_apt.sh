function fApt() {
    dialog --title $titulo --msgbox " 
     Actualizamos el sistema.
    
     También Flatpak y Snap, si los tienes instalados. 
     " 0 0

    dialog --title $titulo --infobox "Actualizando el sistema..." 0 0
    sudo apt update -y >/dev/null 2>&1
    sudo apt upgrade -y >/dev/null 2>&1
    sudo flatpak update >/dev/null 2>&1
    sudo snap refresh >/dev/null 2>&1
}

function fAptCache() {
    dialog --title $titulo --msgbox "
    Borramos versiones antiguas de programas, 
    Kernel, cachés, repositorios no usados, 
    paquetes huerfanos...
    
    " 0 0

    dialog --title $titulo --infobox "Borrando cachés, etc..." 0 0
    sudo apt purge >/dev/null 2>&1
    sudo apt clean -y >/dev/null 2>&1
    sudo apt autoclean -y >/dev/null 2>&1
    sudo apt autoremove -y >/dev/null 2>&1
}